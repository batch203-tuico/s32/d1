const { read } = require("fs");
let http = require("http");
const port = 4000;


// Mock Database
let directory = [
    {
        name: "Brandon",
        email: "brandon@mail.com",
    },
    {
        name: "Jobert",
        email: "jobert@mail.com"
    }
];

const server = http.createServer((req, res) => {
    // When the '/users' route is accessd with a method "GET" we will send the directory (mock database) list of users.
    if (req.url == "/users" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "application/json"});

        res.write(JSON.stringify(directory));

        res.end();
    }

    // We want to received the content of the request and save it to the mock database.
        // 1. Content will be retrieve from the request body.
        // 2. Parse the received JSON request body to JavaScript Object
        // 3. add the parse object to the directory (mock database)
    if (req.url == "/users" && req.method == "POST"){

        // This will act as a placeholder for the resources/data to be created later on
        let  requestBody = "";

        // data step - this reads "data" stream and process it as a request body
        req.on("data", (data) => {
            // data received is on chunk of information
            console.log(data);

            // Assigns the data retrieved from the data stream to requestBody
            // at this point, "requestBody" has the 
            requestBody += data;

            // to show that the chunk of information is stored in the variable requestBody
            console.log(requestBody);
        });

        // response end step - only runs after the request has completely sent.
        req.on("end", () => {
            console.log(typeof requestBody);

            requestBody = JSON.parse(requestBody);

            console.log(typeof requestBody);

            directory.push(requestBody);
            console.log(directory);

            res.writeHead(200, {"Content-Type": "application/json"});
            res.write(JSON.stringify(requestBody));
            res.end();
        });
    }
});

server.listen(port);

console.log(`Server is running at localhost ${port}`);